package riki;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Reducer.Context;

public class MyReducer extends Reducer<Text, Text, Text, Text> {
	
	public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
		
		
		double count = 0;
		boolean found = false;
		
		String arealand = "";
		String areawater = "";
		double perLand = 0;
		double perWater = 0;
		
		String county = "";
		
		
		int countyCount = 0;
		
		// UrbanRural
		double total = 0;
		double urban = 0;
		double rural = 0;
		
		double perUrban = 0;
		double perRural = 0;
		
		// PopulationByAge
		double totalPop = 0;
		double under_5 = 0;
		double from6_18 = 0;
		double seniors = 0;
		
		double perUnder5 = 0;
		double perUnder19 = 0;
		double perSeniors = 0;
		
		// GenderRatio
		double totalSex = 0;
		double maleSex = 0;
		double femaleSex = 0;
		
		
		// MedianAge
		
		double medianIncome = 0;
		double medianFemale = 0;
		double countMedian = 0;
		
		// AvgHouseholdSize
		
		double avgHouseholdzise = 0;
		double totalHouseholds = 0;
		
		// Household Type
		double totalHouseholds1 = 0;
		double single_person = 0;
		double married_couple = 0;
		double non_family = 0;
		
		double perSingle = 0;
		double permarried = 0;
		double perNonFamily = 0;
		
		// HouseholdByPeopleAbove18
		
		double totalHouseholds2 = 0;
		double withOneOrMoreAbove18 = 0;
		double withPeopleAbove60 = 0;
		double withMoreThanOneNonRelative = 0;
		double avgFamilySize = 0;
		
		double perWithAbove18 = 0;
		double perWithAbove60 = 0;
		double perWithNonRelatives = 0;
		
		
		
		for (Text val : values) {
			String content = val.toString();
			
			// Handling header
			if(content.startsWith("$$")) {
				found = true;
				content = content.substring(2);
				
				String tokens[] = content.split(",");
				county = String.valueOf(Integer.parseInt(tokens[0]));
				arealand = tokens[1];
				areawater = tokens[2];
				
				double a = Double.parseDouble(arealand);
				double b = Double.parseDouble(areawater);
				
				perLand = a/(a+b);
				perWater = b/(a+b);
				countyCount++;
				
			} else if(content.startsWith("UrbanRural")) {
				
				String tokens[] = content.split(",");
				
				total+= Double.parseDouble(tokens[1]);
				urban+= Double.parseDouble(tokens[2]);
				rural+= Double.parseDouble(tokens[3]);
				
				perUrban = urban/total;
				perRural = rural/total;
				
			} else if (content.startsWith("PopulationByAge")) {
				
				String tokens[] = content.split(",");
				
				totalPop+= Double.parseDouble(tokens[1]);
				under_5+= Double.parseDouble(tokens[2]);
				from6_18+= Double.parseDouble(tokens[3]);
				seniors+= Double.parseDouble(tokens[4]);
				
				
			} else if (content.startsWith("GenderRatio")) {
				
				String tokens[] = content.split(",");
				
				maleSex += Double.parseDouble(tokens[1]);
				femaleSex += Double.parseDouble(tokens[2]);
				totalSex += Double.parseDouble(tokens[2])+Double.parseDouble(tokens[1]);
				
				
			} else if (content.startsWith("MedianIncome")) {
				
				String tokens[] = content.split(",");
				
				medianIncome+= Double.parseDouble(tokens[1]);
				countMedian++;
				
			} else if (content.startsWith("AvgHouseholdSize")) {
				
				String tokens[] = content.split(",");
				
				avgHouseholdzise+= Double.parseDouble(tokens[1]);
				totalHouseholds+= Double.parseDouble(tokens[2]);
				
			} else if (content.startsWith("HouseholdType")) {
				
				String tokens[] = content.split(",");
				
				totalHouseholds1+= Double.parseDouble(tokens[1]);
				single_person+= Double.parseDouble(tokens[2]);
				married_couple+= Double.parseDouble(tokens[3]);
				non_family+= Double.parseDouble(tokens[4]);
				
				
			} else if (content.startsWith("HouseholdByPeopleAbove18")) {
				
				String tokens[] = content.split(",");
				
				totalHouseholds2+= Double.parseDouble(tokens[1]);
				withOneOrMoreAbove18+= Double.parseDouble(tokens[2]);
				
				perWithAbove18 = withOneOrMoreAbove18/totalHouseholds2;
				
				
			} 
			
		}
		
		if(found == false ) {
			return;
		}
		
		perUnder5 = under_5/totalPop;
		perUnder19 = (under_5+from6_18)/totalPop;
		perSeniors = (seniors)/totalPop;
		
		
		perSingle = single_person/totalHouseholds1;
		permarried = married_couple/totalHouseholds1;
		perNonFamily = non_family/totalHouseholds1;
		
		/*String toWrite = county+ "," +arealand + "," + areawater+ "," + total + "," + urban + "," + rural + "," 
				+totalPop+ "," +under_5+ "," +from5_9+ "," +from10_14+ "," +from15_17+ "," + medianMen 
				+","+medianFemale +","+countMedian +","+avgHouseholdzise +","+totalHouseholds +","+totalHouseholds1 +","+single_person +","+married_couple +","
				+non_family +","+totalHouseholds2 +","+withOneOrMoreAbove18 +","+withPeopleAbove60 +","+withMoreThanOneNonRelative +","+avgFamilySize;*/
		
		String toWrite = total + "," +String.format("%1$.3f", perLand) + "," + String.format("%1$.3f", perWater)+ "," + String.format("%1$.3f", perUrban) + "," + String.format("%1$.3f", perRural) + "," 
				+ String.format("%1$.3f", perUnder5) + "," + String.format("%1$.3f", perUnder19) + "," +String.format("%1$.3f", perSeniors) +","
				+ String.format("%1$.3f", maleSex/totalSex) +"," + String.format("%1$.3f", femaleSex/totalSex)
				+ "," + medianIncome +"," + avgHouseholdzise +","+
				String.format("%1$.3f", perSingle)+","+String.format("%1$.3f", permarried) +","+String.format("%1$.3f", perNonFamily)+","+ 
				String.format("%1$.3f", perWithAbove18);
		
		String keyStr = key.toString();
		String state = keyStr.split(",")[1];
		String logrec = keyStr.split(",")[2];
				
		context.write(new Text(state+","+county), new Text(String.valueOf(toWrite)));
	}

	
	public static void main(String arg[]) throws IOException, InterruptedException {
		String s = "$$ABC";
		System.out.println(s.substring(2));
		
		List<String> sl = new ArrayList<String>();
		sl.add("$$abc,12,13");
		
		MyReducer2 r = new MyReducer2();
		r.reduce(sl);
		
		double l = Double.parseDouble("4.123445576");
		
		System.out.println(String.format("%1$.2f", l));
	}

}
