package riki;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Reducer.Context;

public class MyReducer2 extends Reducer<Text, Text, Text, Text> {
	
	public void reduce(List<String> values) throws IOException, InterruptedException {
		
		
		long count = 0;
		boolean found = false;
		
		String arealand = "";
		String areawater = "";
		String county = "";
		
		int countyCount = 0;
		
		// UrbanRural
		long total = 0;
		long urban = 0;
		long rural = 0;
		
		// PopulationByAge
		long totalPop = 0;
		long under_5 = 0;
		long from6_18 = 0;
		long seniors = 0;
		long from15_17 = 0;
		
		// MedianAge
		
		long medianMen = 0;
		long medianFemale = 0;
		long countMedian = 0;
		
		// AvgHouseholdSize
		
		long avgHouseholdzise = 0;
		long totalHouseholds = 0;
		
		// Household Type
		long totalHouseholds1 = 0;
		long single_person = 0;
		long married_couple = 0;
		long non_family = 0;
		
		// HouseholdByPeopleAbove18
		
		long totalHouseholds2 = 0;
		long withOneOrMoreAbove18 = 0;
		long withPeopleAbove60 = 0;
		long withMoreThanOneNonRelative = 0;
		long avgFamilySize = 0;
		
		
		for (String val : values) {
			String content = val.toString();
			
			// Handling header
			if(content.startsWith("$$")) {
				found = true;
				content = content.substring(2);
				
				String tokens[] = content.split(",");
				county = tokens[0];
				arealand = tokens[1];
				areawater = tokens[2];
				countyCount++;
				
			} else if(content.startsWith("UrbanRural")) {
				
				String tokens[] = content.split(",");
				
				total+= Long.parseLong(tokens[1]);
				urban+= Long.parseLong(tokens[2]);
				rural+= Long.parseLong(tokens[3]);
				
			} else if (content.startsWith("PopulationByAge")) {
				
				String tokens[] = content.split(",");
				
				totalPop+= Long.parseLong(tokens[1]);
				under_5+= Long.parseLong(tokens[2]);
				from6_18+= Long.parseLong(tokens[3]);
				seniors+= Long.parseLong(tokens[4]);
				
				
			} else if (content.startsWith("MedianAge")) {
				
				String tokens[] = content.split(",");
				
				medianMen+= Long.parseLong(tokens[1]);
				medianFemale+= Long.parseLong(tokens[2]);
				countMedian++;
				
			} else if (content.startsWith("AvgHouseholdSize")) {
				
				String tokens[] = content.split(",");
				
				avgHouseholdzise+= Long.parseLong(tokens[1]);
				totalHouseholds+= Long.parseLong(tokens[2]);
				
			} else if (content.startsWith("HouseholdType")) {
				
				String tokens[] = content.split(",");
				
				totalHouseholds1+= Long.parseLong(tokens[1]);
				single_person+= Long.parseLong(tokens[2]);
				married_couple+= Long.parseLong(tokens[2]);
				non_family+= Long.parseLong(tokens[2]);
				
				
			} else if (content.startsWith("HouseholdByPeopleAbove18")) {
				
				String tokens[] = content.split(",");
				
				totalHouseholds2+= Long.parseLong(tokens[1]);
				withOneOrMoreAbove18+= Long.parseLong(tokens[2]);
				withPeopleAbove60+= Long.parseLong(tokens[3]);
				withMoreThanOneNonRelative+= Long.parseLong(tokens[4]);
				avgFamilySize+= Long.parseLong(tokens[5]);
				
			} 
			
		}
		
		if(found == false ) {
			return;
		}
		
		String toWrite = county+ "," +arealand + "," + areawater+ "," + county + "," + countyCount + "," + total + "," + urban + "," + rural + "," 
				+(under_5/totalPop)+ "," +(from6_18/totalPop)+ "," +(seniors/totalPop)+ "," + medianMen 
				+","+medianFemale +","+countMedian +","+avgHouseholdzise +","+totalHouseholds +","+totalHouseholds1 +","+single_person +","+married_couple +","
				+non_family +","+totalHouseholds2 +","+withOneOrMoreAbove18 +","+withPeopleAbove60 +","+withMoreThanOneNonRelative +","+avgFamilySize;
				
		
	}

	private void getGeoLocationData(String content) {
		// TODO Auto-generated method stub
		
	}
	
	public static void main(String arg[]) {
		String s = "$$ABC";
		System.out.println(s.substring(2));
	}

}
