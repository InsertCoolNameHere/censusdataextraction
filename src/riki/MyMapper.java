package riki;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class MyMapper extends Mapper<LongWritable, Text, Text, Text> {
	
	public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
		
		String line = value.toString();
		
		if(line.contains(",") && line.charAt(4) == ',') {
			// This is a data file
			handleDataFile(line , context);
		} else {
			// This is a geo header file
			String summaryLevel = getSummaryLevelFromGeo(line);
			String geoComp = "";
			if(line.startsWith("uSF3")) {
				geoComp = line.substring(11,13);
			}
			
			// Handling only State-County magnification
			if("090".equals(summaryLevel) && geoComp.equals("00")) {
				
				handleGeoFile(line , context);
				
			}
		}
		//context.write(new Text("one"), new Text("1"));
		
	}

	private void handleDataFile(String line, Mapper<LongWritable, Text, Text, Text>.Context context) throws IOException, InterruptedException {
		
		if (line.startsWith("uSF3")) {
			String[] tokens = line.split(",");

			if (tokens != null && tokens.length > 4 && "uSF3".equals(tokens[0])) {

				String identifier = getIdentifierFromDataFile(tokens);

				getDataFromDataFile(tokens, context, identifier);
			}
		}
	}
	
	
	private String getIdentifierFromDataFile(String[] tokens) {
		
		//FILEID,STUSAB,CHARITER,CIFSN,LOGREC
		//CIFSN IS THE FILE NUMBER
		
		String fileId = tokens[0];
		fileId = fileId.trim();
		
		// StateID
		String stusab = tokens[1];
		String logrec = tokens[4];
		
		String identifier = fileId+","+stusab+","+logrec;
		
		return identifier;
		
	}
	
	private void getDataFromDataFile(String[] tokens, Mapper<LongWritable, Text, Text, Text>.Context context, String identifier) throws IOException, InterruptedException {
		
		int type = getFileType(tokens);
		
		if(type == 1) {
			
			handleFile1(tokens, context, identifier);
			handleFile1_5(tokens, context, identifier);
			handleFile1_2(tokens, context, identifier);
			
			
		} else if(type == 7) {
			handleFile7(tokens, context, identifier);
		} else if(type == 56) {
			handleFile56(tokens, context, identifier);
		} 
		
	}
	
	/* HANDLED */
	private void handleFile7(String[] tokens, Mapper<LongWritable, Text, Text, Text>.Context context, String identifier) throws IOException, InterruptedException {
		

		String header="MedianIncome";
		
		String median = tokens[30];
		
		String content = header+","+median.trim();
		
		if(content!=null && !content.isEmpty()) {
			
			context.write(new Text(identifier), new Text(content));
		}
		
		
	}
	
	/* HANDLED */
	private void handleFile56(String[] tokens, Mapper<LongWritable, Text, Text, Text>.Context context, String identifier) throws IOException, InterruptedException {
		

		String header="AvgHouseholdSize";
		
		String avgHouseholdzise = tokens[129];
		String totalHouseholds = tokens[5];
		
		String content = header+","+avgHouseholdzise.trim()+","+totalHouseholds.trim();
		
		if(content!=null && !content.isEmpty()) {
			
			context.write(new Text(identifier), new Text(content));
		}
		
		
	}
	/* HANDLED */
	private void handleFile1_5(String[] tokens, Mapper<LongWritable, Text, Text, Text>.Context context, String identifier) throws IOException, InterruptedException {
		

		String header="HouseholdType";
		
		String totalHouseholds = tokens[147];
		String single_person = tokens[148];
		String married_couple = tokens[153];
		String non_family = tokens[163];
		
		String content = header+","+totalHouseholds.trim()+","+single_person.trim()+","+married_couple+","+non_family;
		
		if(content!=null && !content.isEmpty()) {
			
			context.write(new Text(identifier), new Text(content));
		}
		
		
	}
	
	private void handleFile1_2(String[] tokens, Mapper<LongWritable, Text, Text, Text>.Context context, String identifier) throws IOException, InterruptedException {
		

		String header="HouseholdByPeopleAbove18";
		
		String totalHouseholds = tokens[187];
		int withOneOrMoreunder18 = Integer.parseInt(tokens[191].trim())+Integer.parseInt(tokens[195].trim())+Integer.parseInt(tokens[198].trim())
		+Integer.parseInt(tokens[203].trim())+Integer.parseInt(tokens[206].trim())+Integer.parseInt(tokens[210].trim())+Integer.parseInt(tokens[213].trim());
		
		
		String content = header+","+totalHouseholds.trim()+","+withOneOrMoreunder18;
		
		if(content!=null && !content.isEmpty()) {
			
			context.write(new Text(identifier), new Text(content));
		}
		
		
	}

	/* HANDLED */
	public void handleFile1(String[] tokens, Mapper<LongWritable, Text, Text, Text>.Context context, String identifier) throws IOException, InterruptedException {
		
		String header = "UrbanRural";
		String total = tokens[9];
		String urban = tokens[10];
		String rural = tokens[13];
		
		String content = header+","+total+","+urban+","+rural;
		
		if(content!=null && !content.isEmpty()) {
			
			context.write(new Text(identifier), new Text(content));
		}
		 
		
		String header1="PopulationByAge";
		
		String total1 = tokens[41];
		
		int maleUnder5 = Integer.parseInt(tokens[43])+Integer.parseInt(tokens[44])+Integer.parseInt(tokens[45])+Integer.parseInt(tokens[46])+Integer.parseInt(tokens[47])+
				Integer.parseInt(tokens[48]);
		int male5_18 = Integer.parseInt(tokens[49])+Integer.parseInt(tokens[50])+Integer.parseInt(tokens[51])+Integer.parseInt(tokens[52])+Integer.parseInt(tokens[53])
		+Integer.parseInt(tokens[54])+Integer.parseInt(tokens[55])+Integer.parseInt(tokens[56])+Integer.parseInt(tokens[57])+Integer.parseInt(tokens[58])
		+Integer.parseInt(tokens[59])+Integer.parseInt(tokens[60])+Integer.parseInt(tokens[61]);
		
		int seniors_male = Integer.parseInt(tokens[73])+Integer.parseInt(tokens[74])+Integer.parseInt(tokens[75])+Integer.parseInt(tokens[76])+Integer.parseInt(tokens[77])
		+Integer.parseInt(tokens[78])+Integer.parseInt(tokens[79])+Integer.parseInt(tokens[80]);
		
		int femaleUnder5 = Integer.parseInt(tokens[82])+Integer.parseInt(tokens[83])+Integer.parseInt(tokens[84])+Integer.parseInt(tokens[85])+Integer.parseInt(tokens[86])+
				Integer.parseInt(tokens[87]);
		int female5_18 = Integer.parseInt(tokens[88])+Integer.parseInt(tokens[89])+Integer.parseInt(tokens[90])+Integer.parseInt(tokens[91])+Integer.parseInt(tokens[92])
		+Integer.parseInt(tokens[93])+Integer.parseInt(tokens[94])+Integer.parseInt(tokens[95])+Integer.parseInt(tokens[96])+Integer.parseInt(tokens[97])
		+Integer.parseInt(tokens[98])+Integer.parseInt(tokens[99])+Integer.parseInt(tokens[100]);
		
		int seniors_female = Integer.parseInt(tokens[112])+Integer.parseInt(tokens[113])+Integer.parseInt(tokens[114])+Integer.parseInt(tokens[115])+Integer.parseInt(tokens[116])
		+Integer.parseInt(tokens[117])+Integer.parseInt(tokens[118])+Integer.parseInt(tokens[119]);
		
		
		int under5 = maleUnder5+femaleUnder5;
		int from6_18 = male5_18+female5_18;
		int seniors = seniors_male+seniors_female;
		
		String content1 = header1+","+total1+","+under5+","+from6_18+","+seniors;
		
		if(content1!=null && !content1.isEmpty()) {
			
			context.write(new Text(identifier), new Text(content1));
		}
		
		
		String header2 = "GenderRatio";
		String male = tokens[42];
		String female = tokens[81];
		
		
		String content2 = header2+","+male+","+female;
		
		if(content2!=null && !content2.isEmpty()) {
			
			context.write(new Text(identifier), new Text(content2));
		}
		
	}

	private int getFileType(String[] tokens) {
		
		int type = Integer.parseInt(tokens[3]);
		
		return type;
		
	}
	
	private void handleGeoFile(String line, Mapper<LongWritable, Text, Text, Text>.Context context) throws IOException, InterruptedException {
		
		String identifier = getIdentifierFromGeo(line);
		
		// EXTARCT LAND,WATER INFO
		
		String info = landwaterInfo(line);
		
		if(info != null)
			context.write(new Text(identifier), new Text(info));
		
	}
	
	
	private String landwaterInfo(String line) {
		
		String county = line.substring(31,34);
		String arealand = line.substring(172,186);
		String areawater = line.substring(186, 200);
		
		if(county == null || county.isEmpty()) {
			return null;
		}
		
		String s = "$$"+county+","+arealand.trim()+","+areawater.trim();
		return s;
	}
	
	private String getIdentifierFromGeo(String line) {
		
		//FILEID,STUSAB,CHARITER,CIFSN,LOGREC
		//CIFSN IS THE FILE NUMBER
		
		String fileId = line.substring(0,6);
		fileId = fileId.trim();
		
		// StateID
		String stusab = line.substring(6,8);
		String logrec = line.substring(18,25);
		
		String identifier = fileId+","+stusab+","+logrec;
		
		return identifier;
		
	}
	
	private String getSummaryLevelFromGeo(String line) {
		if(line.startsWith("uSF3")) {
			String summaryLevel = line.substring(8,11);
			return summaryLevel;
		}
		return null;
	}
	
	
	public static void main(String arg[]) {
		String g  = "uSF3,AK,000,01,0000001,626932,127412,626932,20.3,626932,411955,278013,133942,214977,1224,213753,626932,434225,21968,97012,25496,3122,9869,35240,626932,601167,423660,21124,95345,25199,3013,1273,31553,25765,10565,844,1667,297,109,8596,3687,626932,324282,4811,4845,4673,4875,5084,5011,5164,5646,5673,6348,5982,5683,5868,5773,5822,5664,5433,5934,4723,4342,4651,4576,12025,22237,23980,28922,30265,27993,22092,14766,4167,4827,2881,3431,4722,2974,1454,965,302650,4441,4665,4526,4599,4673,4831,4930,4953,5563,5798,5678,5594,5790,5363,5409,5143,5343,4919,4075,3552,3831,3859,11235,20660,22332,27203,29304,25637,19159,12889,3325,4705,2614,3500,4832,3705,2291,1724,626932,607641,519469,153611,115281,38330,119546,206852,186102,8384,12366,8812,3662,2896,7591,16499,88172,39610,29786,9824,28583,22164,6419,19979,19291,4952,14339,221804,51950,29786,22164,169854,153611,118332,65028,53304,35279,11822,7917,3905,23457,16892,6565,16243,9824,6419,35093,33387,23477,12942,10310,2632,7364,1625,1323,223,9910,3431,3076,355,6048,5759,289,431,1706,792,914,221804,199383,140669,108341,64648,43693,32328,10967,7828,3139,21361,16801,4560,58714,43115,15599,22421,12942,9991,380,9611,2951,855,89,766,2096,91,2005,9479,8835,644,221804,153611,7202,29649,46399,39014,18405,8846,3478,618,68193,6188,12315,15109,16190,8912,5353,3256,870,221804,153611,54039,36363,33800,16813,7339,5257,68193,51950,13706,1814,520,172,23,8";
		System.out.println(g.charAt(4));
	}
	
}
