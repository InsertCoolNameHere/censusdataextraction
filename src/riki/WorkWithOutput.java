package riki;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

public class WorkWithOutput {
	
	Map<String, String> stateToInfo = new TreeMap<String, String>();

	public static void main(String[] args) throws IOException {
		
		WorkWithOutput w = new WorkWithOutput();
		w.readFile("/s/chopin/b/grad/sapmitra/demo/op1/total");

	}

	
	public void readFile(String filename) throws IOException {
		try {
			File file = new File(filename);
			// READING VERTICES
			String sCurrentLine;

			BufferedReader br = new BufferedReader(new FileReader(file));

			while ((sCurrentLine = br.readLine()) != null) {
				if (sCurrentLine.contains("\t")) {
					
					String tokens[] = sCurrentLine.split("\\s+");
					
					if(tokens.length != 2) {
						System.out.println("=======PROBLEM WITH "+sCurrentLine+"========");
						continue;
					}
					
					// handle key
					String key = tokens[0];
					String[] keyTokens = key.split(",");
					String stateCode = keyTokens[1];
					
					//handle content
					
					String content = tokens[1];
					String[] contentTokens = content.split(",");
					
					String countyCode = contentTokens[0];
					
					String mapKey = stateCode+","+countyCode;
					
					if(stateToInfo.get(mapKey) != null) {
						
						System.out.println("DUPLICATE ENTRY "+mapKey);
						
					} else {
						
						//arealand1,areawater2,totalhouses5,urban6,rural7,
						stateToInfo.put(mapKey, "demo");
					}
				}
			}
			
			br.close();

			

		} catch (IOException e) {

			e.printStackTrace();

		}

	}
	
	
	

}
